package com.atlassian.jira.rest.client.internal.json;
import com.atlassian.jira.rest.client.api.domain.TotalCount;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class TotalCountJsonParser implements JsonObjectParser<TotalCount>{
    @Override
    public TotalCount parse(JSONObject json) throws JSONException {
        final int count =  json.getInt("count");
        return new TotalCount(count);
    }
}

