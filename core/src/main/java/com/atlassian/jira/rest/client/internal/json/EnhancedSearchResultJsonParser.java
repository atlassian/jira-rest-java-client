package com.atlassian.jira.rest.client.internal.json;

import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.SearchResult;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class EnhancedSearchResultJsonParser extends SearchResultJsonParser implements JsonObjectParser<SearchResult> {
    @Override
    public SearchResult parse(JSONObject json) throws JSONException {
        final JSONArray issuesJsonArray = json.getJSONArray("issues");
        final String nextPageToken = json.has("nextPageToken")? json.getString("nextPageToken"): null;
        final Iterable<Issue> issues = parseIssues(json, issuesJsonArray);
        return new SearchResult(issues, nextPageToken);
    }
}
