package com.atlassian.jira.rest.client.api.domain;

public class TotalCount {
    private int count;

    public TotalCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "TotalCountResult{" +
                "count=" + count +
                '}';
    }
}
