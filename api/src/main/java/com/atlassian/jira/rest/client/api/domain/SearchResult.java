/*
 * Copyright (C) 2011 Atlassian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.jira.rest.client.api.domain;

import java.util.Objects;

/**
 * Represents search results - links to issues matching given filter (JQL query) with basic
 * information supporting the paging through the results.
 *
 * @since v0.2
 */
public class SearchResult {

    private int startIndex;
    private int total;
    private int maxResults;
    private String nextPageToken;
    private final Iterable<Issue> issues;
    private final boolean isCloudResult;

    public SearchResult(int startIndex, int maxResults, int total, Iterable<Issue> issues) {
        this.startIndex = startIndex;
        this.maxResults = maxResults;
        this.total = total;
        this.issues = issues;
        isCloudResult = false;
    }

    public SearchResult(Iterable<Issue> issues, String nextPageToken) {
        this.issues = issues;
        this.nextPageToken = nextPageToken;
        isCloudResult = true;
    }

    /**
     * @return 0-based start index of the returned issues (e.g. "3" means that 4th, 5th...maxResults issues matching given query
     * have been returned.
     * Available only in DC. Use `nextPageToken` to fetch the next page.
     */
    public int getStartIndex() {
        if(isCloudResult) {
            throw new UnsupportedOperationException("Start index is not available in the Cloud version of the new Enhanced Search API response. Use `nextPageToken` to fetch the next page. If there are no more pages, `nextPageToken` will be null.");
        }
        return startIndex;
    }

    /**
     * @return maximum page size (the window to results).
     * Available only in DC.
     */
    public int getMaxResults() {
        if(isCloudResult) {
            throw new UnsupportedOperationException("MaxResults is not available in the Cloud version of the new Enhanced Search API response.");
        }
        return maxResults;
    }

    /**
     * @return total number of issues (regardless of current maxResults and startIndex) matching given criteria.
     * Query JIRA another time with different startIndex to get subsequent issues.
     * Available only in DC, To get total count use #SearchRestClient.totalCount to fetch the estimated count of the issues for a given JQL.
     */
    public int getTotal() {
        if(isCloudResult) {
            throw new UnsupportedOperationException("Total is not available in the Cloud version of the new Search API response. Please use `SearchRestClient.totalCount` instead to fetch the estimated count of the issues for a given JQL.");
        }
        return total;
    }

    public Iterable<Issue> getIssues() {
        return issues;
    }

    /**
     * @return token to get next pages of results. Available only in Cloud version. For DC use #getStartIndex() and #getMaxResults()
     */
    public String getNextPageToken() {
        if(!isCloudResult){
            throw new UnsupportedOperationException("NextPageToken is not available in the DC version of the new Enhanced Search API response. Use `getStartIndex()` and `getMaxResults()` to fetch the next page.");
        }
        return nextPageToken;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        SearchResult that = (SearchResult) o;
        return startIndex == that.startIndex && total == that.total && maxResults == that.maxResults && Objects.equals(nextPageToken, that.nextPageToken) && Objects.equals(issues, that.issues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startIndex, total, maxResults, nextPageToken, issues);
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "startIndex=" + startIndex +
                ", total=" + total +
                ", maxResults=" + maxResults +
                ", nextPageToken='" + nextPageToken + '\'' +
                ", issues=" + issues +
                '}';
    }
}
